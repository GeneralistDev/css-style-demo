import * as React from 'react';
import { SignupForm as SignupFormUtility } from '../components/utilityForm';
import { SignupForm as SignupFormBEM } from '../components/bemForm';

export const FormDemo = () => {
  return <div className="flex flex-row">
    <SignupFormUtility className="w-full mr-2"/>
    <SignupFormBEM className="w-full ml-2" />
  </div>
};