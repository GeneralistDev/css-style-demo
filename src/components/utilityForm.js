import { Formik } from 'formik';
import * as React from 'react';
import * as Yup from 'yup';

export class SignupForm extends React.Component {
  render() {
    const validationSchema = Yup.object().shape({
      firstName: Yup.string().required('Required'),
      lastName: Yup.string().required('Required'),
      userName: Yup.string().required('Required')
    });

    return (
      <div className={this.props.className}>
        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            userName: ''
          }}
          onSubmit={() => {
            alert('form submitted');
          }}
          validationSchema={validationSchema}>
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            errors
          }) => (
            <div>
              <h3>Utility-First</h3>
              <form className="flex flex-col text-black" onSubmit={handleSubmit}>
                <div className="mb-2 h-12 text-left">
                  <input 
                    type="text"
                    className="p-1 pl-2 rounded"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstName}
                    name="firstName"
                    placeholder="First Name"
                  />
                  {errors.firstName && touched.firstName ? (
                    <div className="text-red-600 text-sm ml-1 font-bold">{errors.firstName}</div>
                  ) : null}
                </div>
                
                <div className="mb-2 h-12 text-left">
                  <input 
                    type="text"
                    className="p-1 pl-2 rounded"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                    name="lastName"
                    placeholder="Last Name"
                  />
                  {errors.lastName && touched.lastName ? (
                    <div className="text-red-600 text-sm ml-1 font-bold">{errors.lastName}</div>
                  ) : null}
                </div>
                
                <div className="mb-2 h-12 text-left">
                  <input 
                    type="text"
                    className="p-1 pl-2 rounded"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.userName}
                    name="userName"
                    placeholder="User Name"
                  />
                  {errors.userName && touched.userName ? (
                    <div className="text-red-600 text-sm ml-1 font-bold">{errors.userName}</div>
                  ) : null}
                </div>
                
                <button className="text-white my-2 bg-green-500 hover:bg-green-700 rounded p-2" type="submit">Submit</button>
              </form>
            </div>
          )}
        </Formik>
      </div>
    );
  }
};