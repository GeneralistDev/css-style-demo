import { Formik } from 'formik';
import * as React from 'react';
import * as Yup from 'yup';
import './bemForm.scss';

export class SignupForm extends React.Component {
  render() {
    const validationSchema = Yup.object().shape({
      firstName: Yup.string().required('Required'),
      lastName: Yup.string().required('Required'),
      userName: Yup.string().required('Required')
    });

    return (
      <div className={this.props.className}>
        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            userName: ''
          }}
          onSubmit={() => {
            alert('form submitted');
          }}
          validationSchema={validationSchema}>
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            errors
          }) => (
            <div className="bemForm">
              <h3>BEM</h3>
              <form onSubmit={handleSubmit}>
                <div className="bemForm__inputGroup">
                  <input 
                    type="text"
                    className="bemForm__input"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstName}
                    name="firstName"
                    placeholder="First Name"
                  />
                  {errors.firstName && touched.firstName ? (
                    <div className="bemForm__inputError">{errors.firstName}</div>
                  ) : null}
                </div>
                
                <div className="bemForm__inputGroup">
                  <input 
                    type="text"
                    className="bemForm__input"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                    name="lastName"
                    placeholder="Last Name"
                  />
                  {errors.lastName && touched.lastName ? (
                    <div className="bemForm__inputError">{errors.lastName}</div>
                  ) : null}
                </div>
                
                <div className="bemForm__inputGroup">
                  <input 
                    type="text"
                    className="bemForm__input"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.userName}
                    name="userName"
                    placeholder="User Name"
                  />
                  {errors.userName && touched.userName ? (
                    <div className="bemForm__inputError">{errors.userName}</div>
                  ) : null}
                </div>
                
                <button className="bemForm__submitButton" type="submit">Submit</button>
              </form>
            </div>
          )}
        </Formik>
      </div>
      
    );
  }
};