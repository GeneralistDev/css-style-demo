
import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import styles from './button.module.scss';

export const ButtonVariants = {
  default: 'button--default',
  primary: 'button--primary',
  secondary: 'button--secondary'
};

export class Button extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      variant: props.variant ? props.variant : ButtonVariants.default
    };
  }

  render() {
    const buttonStyle = styles[this.state.variant];

    return (
      <button 
        {...this.props}
        className={cx(buttonStyle, this.props.className)}>{this.props.children}</button>
    );
  }
}

Button.propTypes = {
  variant: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func
};