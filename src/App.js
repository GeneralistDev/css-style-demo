import React from 'react';
import './base.css';
import './App.scss';
import { Button, ButtonVariants } from './components/button';
import { FormDemo } from './containers/formDemo';

function App() {
  function click() {
    alert('clicked');
  }

  return (
    <div className="App">
      <header className="pt-20 text-white flex flex-col w-full items-center">
        <div className="flex w-1/2 flex-row">
          <div className="flex flex-col flex-1 mx-10">
            <h1>BEM buttons</h1>
            <Button onClick={click}>Default</Button>
            <Button onClick={click} variant={ButtonVariants.primary}>Primary</Button>
            <Button onClick={click} variant={ButtonVariants.secondary}>Secondary</Button>
            <Button onClick={click} className="App__button--custom1">BEM Custom 1</Button>
          </div>
          <div className="flex flex-col flex-1 mx-10">
            <h1>Utility First (tailwindcss)</h1>
            <Button onClick={click} className="hover:bg-gray-400 rounded">Default</Button>
            <Button onClick={click} className="bg-gray-700 hover:bg-gray-600 rounded text-white">Custom 1</Button>
            <Button onClick={click} className="bg-red-600 hover:bg-red-500 rounded text-white">Custom 2</Button>
          </div>
        </div>
        <div className="flex flex-col w-1/2 items-center mt-4">
          <h1>Form demo</h1>
          <FormDemo />
        </div>
      </header>
    </div>
  );
}

export default App;
